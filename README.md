# Arduino WireSafe Library #

This is a modified version of the standard Arduino Wire library which won't lock-up your application in the case of I2C hardware errors.

### How do I get set up? ###

* Create a WireSafe folder in Arduino libraries directory.  Place all the code from this repository into that folder.
* use WireSafe.<whatever> in your Arduino code.