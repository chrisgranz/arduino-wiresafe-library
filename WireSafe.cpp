/*
  TwoWireSafe.cpp - TWI/I2C library for Arduino
  Copyright (c) 2006 Nicholas Zambetti.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 
  Modified 2012 by Todd Krein (todd@krein.org) to implement repeated starts
  
  Modified September 2013 by Christopher Granz to include timeouts.
*/

#if defined(__MK20DX128__)

#include "mk20dx128.h"
#include "core_pins.h"
#include "HardwareSerial.h"
#include "WireSafe.h"

uint8_t TwoWireSafe::rxBuffer[BUFFER_LENGTH];
uint8_t TwoWireSafe::rxBufferIndex = 0;
uint8_t TwoWireSafe::rxBufferLength = 0;
//uint8_t TwoWireSafe::txAddress = 0;
uint8_t TwoWireSafe::txBuffer[BUFFER_LENGTH+1];
//uint8_t TwoWireSafe::txBufferIndex = 0;
uint8_t TwoWireSafe::txBufferLength = 0;
uint8_t TwoWireSafe::transmitting = 0;
//void (*TwoWireSafe::user_onRequest)(void);
//void (*TwoWireSafe::user_onReceive)(int);


TwoWireSafe::TwoWireSafe()
{
}

void TwoWireSafe::begin(void)
{
	//serial_begin(BAUD2DIV(115200));
	//serial_print("\nWire Begin\n");

	SIM_SCGC4 |= SIM_SCGC4_I2C0; // TODO: use bitband
	// On Teensy 3.0 external pullup resistors *MUST* be used
	// the PORT_PCR_PE bit is ignored when in I2C mode
	// I2C will not work at all without pullup resistors
	CORE_PIN18_CONFIG = PORT_PCR_MUX(2)|PORT_PCR_PE|PORT_PCR_PS;
	CORE_PIN19_CONFIG = PORT_PCR_MUX(2)|PORT_PCR_PE|PORT_PCR_PS;
#if F_BUS == 48000000
	I2C0_F = 0x27;	// 100 kHz
	// I2C0_F = 0x1A; // 400 kHz
	// I2C0_F = 0x0D; // 1 MHz
	I2C0_FLT = 4;
#elif F_BUS == 24000000
	I2C0_F = 0x1F; // 100 kHz
	// I2C0_F = 0x45; // 400 kHz
	// I2C0_F = 0x02; // 1 MHz
	I2C0_FLT = 2;
#else
#error "F_BUS must be 48 MHz or 24 MHz"
#endif
	I2C0_C2 = I2C_C2_HDRS;
	I2C0_C1 = I2C_C1_IICEN;
}

// Chapter 44: Inter-Integrated Circuit (I2C) - Page 1012
//  I2C0_A1      // I2C Address Register 1
//  I2C0_F       // I2C Frequency Divider register
//  I2C0_C1      // I2C Control Register 1
//  I2C0_S       // I2C Status register
//  I2C0_D       // I2C Data I/O register
//  I2C0_C2      // I2C Control Register 2
//  I2C0_FLT     // I2C Programmable Input Glitch Filter register


// returns 0 on success, -1 on timeout
static int i2c_wait(int timeout)
{
	while (!(I2C0_S & I2C_S_IICIF) && timeout > 0) // wait for hardware flag
        timeout--;

	I2C0_S = I2C_S_IICIF;
}

void TwoWireSafe::beginTransmission(uint8_t address)
{
	txBuffer[0] = (address << 1);
	transmitting = 1;
	txBufferLength = 1;
}

size_t TwoWireSafe::write(uint8_t data)
{
	if (transmitting) {
		if (txBufferLength >= BUFFER_LENGTH+1) {
			setWriteError();
			return 0;
		}
		txBuffer[txBufferLength++] = data;
	} else {
		// TODO: implement slave mode
	}
}

size_t TwoWireSafe::write(const uint8_t *data, size_t quantity)
{
	if (transmitting) {
		while (quantity > 0) {
			write(*data++);
			quantity--;
		}
	} else {
		// TODO: implement slave mode
	}
	return 0;
}

void TwoWireSafe::flush(void)
{
}


uint8_t TwoWireSafe::endTransmission(uint8_t sendStop)
{
	uint8_t i, status;

	// clear the status flags
	I2C0_S = I2C_S_IICIF | I2C_S_ARBL;
	// now take control of the bus...
	if (I2C0_C1 & I2C_C1_MST) {
		// we are already the bus master, so send a repeated start
		I2C0_C1 = I2C_C1_IICEN | I2C_C1_MST | I2C_C1_RSTA | I2C_C1_TX;
	} else {
		// we are not currently the bus master, so wait for bus ready
		while (I2C0_S & I2C_S_BUSY) ;
		// become the bus master in transmit mode (send start)
		I2C0_C1 = I2C_C1_IICEN | I2C_C1_MST | I2C_C1_TX;
	}
	// transmit the address and data
	for (i=0; i < txBufferLength; i++) {
		I2C0_D = txBuffer[i];
		i2c_wait();
		status = I2C0_S;
		if (status & I2C_S_RXAK) {
			// the slave device did not acknowledge
			break;
		}
		if ((status & I2C_S_ARBL)) {
			// we lost bus arbitration to another master
			// TODO: what is the proper thing to do here??
			break;
		}
	}
	if (sendStop) {
		// send the stop condition
		I2C0_C1 = I2C_C1_IICEN;
		// TODO: do we wait for this somehow?
	}
	transmitting = 0;
	return 0;
}


int8_t TwoWireSafe::requestFrom(uint8_t address, uint8_t length, uint8_t sendStop, uint16_t timeout)
{
	uint8_t tmp, status, count=0;
    uint16_t timer;
    int error = 0;

	rxBufferIndex = 0;
	rxBufferLength = 0;
	//serial_print("requestFrom\n");
	// clear the status flags
	I2C0_S = I2C_S_IICIF | I2C_S_ARBL;
	// now take control of the bus...
	if (I2C0_C1 & I2C_C1_MST) {
		// we are already the bus master, so send a repeated start
		I2C0_C1 = I2C_C1_IICEN | I2C_C1_MST | I2C_C1_RSTA | I2C_C1_TX;
	} else {
		// we are not currently the bus master, so wait for bus ready
        timer = 0;
		while ((I2C0_S & I2C_S_BUSY) && timer < timeout)
            timer++;;
            
        if (timer == timeout)
            error = 1;

		// become the bus master in transmit mode (send start)
		I2C0_C1 = I2C_C1_IICEN | I2C_C1_MST | I2C_C1_TX;
	}
	// send the address
	I2C0_D = (address << 1) | 1;
	error |= i2c_wait(timeout);
	status = I2C0_S;
	if ((status & I2C_S_RXAK) || (status & I2C_S_ARBL)) {
		// the slave device did not acknowledge
		// or we lost bus arbitration to another master
		I2C0_C1 = I2C_C1_IICEN;
		return 0;
	}
	if (length == 0) {
		// TODO: does anybody really do zero length reads?
		// if so, does this code really work?
		I2C0_C1 = I2C_C1_IICEN | (sendStop ? 0 : I2C_C1_MST);
		return 0;
	} else if (length == 1) {
		I2C0_C1 = I2C_C1_IICEN | I2C_C1_MST | I2C_C1_TXAK;
	} else {
		I2C0_C1 = I2C_C1_IICEN | I2C_C1_MST;
	}
	tmp = I2C0_D; // initiate the first receive
	while (length > 1) {
		error |= i2c_wait(timeout);
		length--;
		if (length == 1) I2C0_C1 = I2C_C1_IICEN | I2C_C1_MST | I2C_C1_TXAK;
		rxBuffer[count++] = I2C0_D;
	}
	error |= i2c_wait(timeout);
	I2C0_C1 = I2C_C1_IICEN | I2C_C1_MST | I2C_C1_TX;
	rxBuffer[count++] = I2C0_D;
	if (sendStop) I2C0_C1 = I2C_C1_IICEN;
	rxBufferLength = count;

    if (error)
        return -1;
    else
        return count;
}

int TwoWireSafe::available(void)
{
	return rxBufferLength - rxBufferIndex;
}

int TwoWireSafe::read(void)
{
	if (rxBufferIndex >= rxBufferLength) return -1;
	return rxBuffer[rxBufferIndex++];
}

int TwoWireSafe::peek(void)
{
	if (rxBufferIndex >= rxBufferLength) return -1;
	return rxBuffer[rxBufferIndex];
}

uint8_t TwoWireSafe::requestFrom(uint8_t address, uint8_t quantity)
{
  return requestFrom((uint8_t)address, (uint8_t)quantity, (uint8_t)true);
}

uint8_t TwoWireSafe::requestFrom(int address, int quantity)
{
  return requestFrom((uint8_t)address, (uint8_t)quantity, (uint8_t)true);
}

uint8_t TwoWireSafe::requestFrom(int address, int quantity, int sendStop)
{
  return requestFrom((uint8_t)address, (uint8_t)quantity, (uint8_t)sendStop);
}

void TwoWireSafe::beginTransmission(int address)
{
	beginTransmission((uint8_t)address);
}

uint8_t TwoWireSafe::endTransmission(void)
{
	return endTransmission(true);
}

void i2c0_isr(void)
{
}

//TwoWireSafe Wire = TwoWireSafe();
TwoWireSafe WireSafe;

#endif // __MK20DX128__



#if defined(__AVR__)

extern "C" {
  #include <stdlib.h>
  #include <string.h>
  #include <inttypes.h>
  #include "twi_safe.h"
}

#include "WireSafe.h"

// Initialize Class Variables //////////////////////////////////////////////////

uint8_t TwoWireSafe::rxBuffer[BUFFER_LENGTH];
uint8_t TwoWireSafe::rxBufferIndex = 0;
uint8_t TwoWireSafe::rxBufferLength = 0;

uint8_t TwoWireSafe::txAddress = 0;
uint8_t TwoWireSafe::txBuffer[BUFFER_LENGTH];
uint8_t TwoWireSafe::txBufferIndex = 0;
uint8_t TwoWireSafe::txBufferLength = 0;

uint8_t TwoWireSafe::transmitting = 0;
void (*TwoWireSafe::user_onRequest)(void);
void (*TwoWireSafe::user_onReceive)(int);

// Constructors ////////////////////////////////////////////////////////////////

TwoWireSafe::TwoWireSafe()
{
}

// Public Methods //////////////////////////////////////////////////////////////

void TwoWireSafe::begin(bool pullups)
{
  rxBufferIndex = 0;
  rxBufferLength = 0;

  txBufferIndex = 0;
  txBufferLength = 0;

  twi_safe_init((uint8_t)pullups); // turn on internal pullups?
}

void TwoWireSafe::begin(uint8_t address, bool pullups)
{
  twi_safe_setAddress(address);
  twi_safe_attachSlaveTxEvent(onRequestService);
  twi_safe_attachSlaveRxEvent(onReceiveService);
  begin(pullups);
}

void TwoWireSafe::begin(int address, bool pullups)
{
  begin((uint8_t)address, pullups);
}

int8_t TwoWireSafe::requestFrom(uint8_t address, uint8_t quantity, uint8_t sendStop, uint16_t timeout)
{
  // clamp to buffer length
  if(quantity > BUFFER_LENGTH){
    quantity = BUFFER_LENGTH;
  }
  // perform blocking read into buffer (up to timeout counts)
  int8_t read = twi_safe_readFrom(address, rxBuffer, quantity, sendStop, timeout);
  // set rx buffer iterator vars
  rxBufferIndex = 0;
  rxBufferLength = (read >= 0 ? read : 0);

  return read;
}

int8_t TwoWireSafe::requestFrom(uint8_t address, uint8_t quantity)
{
  return requestFrom((uint8_t)address, (uint8_t)quantity, (uint8_t)true, TWI_STANDARD_TIMEOUT);
}

int8_t TwoWireSafe::requestFrom(int address, int quantity)
{
  return requestFrom((uint8_t)address, (uint8_t)quantity, (uint8_t)true, TWI_STANDARD_TIMEOUT);
}

int8_t TwoWireSafe::requestFrom(int address, int quantity, int sendStop)
{
  return requestFrom((uint8_t)address, (uint8_t)quantity, (uint8_t)sendStop, TWI_STANDARD_TIMEOUT);
}

void TwoWireSafe::beginTransmission(uint8_t address)
{
  // indicate that we are transmitting
  transmitting = 1;
  // set address of targeted slave
  txAddress = address;
  // reset tx buffer iterator vars
  txBufferIndex = 0;
  txBufferLength = 0;
}

void TwoWireSafe::beginTransmission(int address)
{
  beginTransmission((uint8_t)address);
}

//
//	Originally, 'endTransmission' was an f(void) function.
//	It has been modified to take one parameter indicating
//	whether or not a STOP should be performed on the bus.
//	Calling endTransmission(false) allows a sketch to 
//	perform a repeated start. 
//
//	WARNING: Nothing in the library keeps track of whether
//	the bus tenure has been properly ended with a STOP. It
//	is very possible to leave the bus in a hung state if
//	no call to endTransmission(true) is made. Some I2C
//	devices will behave oddly if they do not see a STOP.
//
int8_t TwoWireSafe::endTransmission(uint8_t sendStop, uint16_t timeout)
{
  // transmit buffer (blocking)
  int8_t ret = twi_safe_writeTo(txAddress, txBuffer, txBufferLength, 1, sendStop, timeout);
  // reset tx buffer iterator vars
  txBufferIndex = 0;
  txBufferLength = 0;
  // indicate that we are done transmitting
  transmitting = 0;
  return ret;
}

int8_t TwoWireSafe::endTransmission(uint8_t sendStop)
{
  return endTransmission(sendStop, TWI_STANDARD_TIMEOUT);
}

//	This provides backwards compatibility with the original
//	definition, and expected behaviour, of endTransmission
//
int8_t TwoWireSafe::endTransmission(void)
{
  return endTransmission(true, TWI_STANDARD_TIMEOUT);
}

// must be called in:
// slave tx event callback
// or after beginTransmission(address)
size_t TwoWireSafe::write(uint8_t data)
{
  if(transmitting){
  // in master transmitter mode
    // don't bother if buffer is full
    if(txBufferLength >= BUFFER_LENGTH){
      setWriteError();
      return 0;
    }
    // put byte in tx buffer
    txBuffer[txBufferIndex] = data;
    ++txBufferIndex;
    // update amount in buffer   
    txBufferLength = txBufferIndex;
  }else{
  // in slave send mode
    // reply to master
    twi_safe_transmit(&data, 1);
  }
  return 1;
}

// must be called in:
// slave tx event callback
// or after beginTransmission(address)
size_t TwoWireSafe::write(const uint8_t *data, size_t quantity)
{
  if(transmitting){
  // in master transmitter mode
    for(size_t i = 0; i < quantity; ++i){
      write(data[i]);
    }
  }else{
  // in slave send mode
    // reply to master
    twi_safe_transmit(data, quantity);
  }
  return quantity;
}

// must be called in:
// slave rx event callback
// or after requestFrom(address, numBytes)
int TwoWireSafe::available(void)
{
  return rxBufferLength - rxBufferIndex;
}

// must be called in:
// slave rx event callback
// or after requestFrom(address, numBytes)
int TwoWireSafe::read(void)
{
  int value = -1;
  
  // get each successive byte on each call
  if(rxBufferIndex < rxBufferLength){
    value = rxBuffer[rxBufferIndex];
    ++rxBufferIndex;
  }

  return value;
}

// must be called in:
// slave rx event callback
// or after requestFrom(address, numBytes)
int TwoWireSafe::peek(void)
{
  int value = -1;
  
  if(rxBufferIndex < rxBufferLength){
    value = rxBuffer[rxBufferIndex];
  }

  return value;
}

void TwoWireSafe::flush(void)
{
  // XXX: to be implemented.
}

// behind the scenes function that is called when data is received
void TwoWireSafe::onReceiveService(uint8_t* inBytes, int numBytes)
{
  // don't bother if user hasn't registered a callback
  if(!user_onReceive){
    return;
  }
  // don't bother if rx buffer is in use by a master requestFrom() op
  // i know this drops data, but it allows for slight stupidity
  // meaning, they may not have read all the master requestFrom() data yet
  if(rxBufferIndex < rxBufferLength){
    return;
  }
  // copy twi rx buffer into local read buffer
  // this enables new reads to happen in parallel
  for(uint8_t i = 0; i < numBytes; ++i){
    rxBuffer[i] = inBytes[i];    
  }
  // set rx iterator vars
  rxBufferIndex = 0;
  rxBufferLength = numBytes;
  // alert user program
  user_onReceive(numBytes);
}

// behind the scenes function that is called when data is requested
void TwoWireSafe::onRequestService(void)
{
  // don't bother if user hasn't registered a callback
  if(!user_onRequest){
    return;
  }
  // reset tx buffer iterator vars
  // !!! this will kill any pending pre-master sendTo() activity
  txBufferIndex = 0;
  txBufferLength = 0;
  // alert user program
  user_onRequest();
}

// sets function called on slave write
void TwoWireSafe::onReceive( void (*function)(int) )
{
  user_onReceive = function;
}

// sets function called on slave read
void TwoWireSafe::onRequest( void (*function)(void) )
{
  user_onRequest = function;
}

// Preinstantiate Objects //////////////////////////////////////////////////////

TwoWireSafe WireSafe = TwoWireSafe();

#endif // __AVR__
